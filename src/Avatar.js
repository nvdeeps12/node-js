import React from 'react';
import 'tachyons';
import Avatarlist from './Avatarlist.js';

const avtrarray = [
	{
		id: '1',
		name: 'Navdeep Singh',
		occ: 'Backend Developer'
	},

	{
		id: '2',
		name: 'Kamaljeet Singh',
		occ: 'Backend Developer'
	},

	{
		id: '3',
		name: 'Navjot Singh',
		occ: 'Backend Developer'
	},
	{
		id: '4',
		name: 'Rajkamal Singh',
		occ: 'Backend Developer'
	}
];

// let block
// const block
// var  function

const Avatar = () => {
	return (
		<div>
			<h1 className="tc">Welcome to Avatar World!!</h1>

			{/* This is from array list.js file.
			 we are passing names through props like array[0]name for name */}

			<Avatarlist name={avtrarray[0].name} occ={avtrarray[0].occ} />
			<Avatarlist name={avtrarray[1].name} occ={avtrarray[1].occ} />
			<Avatarlist name={avtrarray[2].name} occ={avtrarray[2].occ} />
			<Avatarlist name={avtrarray[3].name} occ={avtrarray[3].occ} />

			<button className="f6 grow no-underline br-pill ph3 pv2 mb2 dib white bg-black">Click Me</button>
		</div>
	);
};

export default Avatar;
