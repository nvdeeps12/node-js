function custom_hello(name) {
	// var has outside function scope
	// let has inside a block scope only
	// i is outside of block and is accessible because of var keyword
	// Arguments can be passed in the function directly same like python

	for (var i = 0; i < 5; i++)
		if (i > 2) {
			console.log('I am Greater than 2 and I am Here ' + name + i);
		}

	if (true) {
		var color = 'Red';
	}

	console.log(color);
}

custom_hello('Navdeep Singh-->');

function arrfunc() {
	arr = [ 'Navdeep', 'Kamal' ];
	console.log(arr[1]);
}

arrfunc();

// This is the normal javascript function
const squares = function(number) {
	return number * number;
};

// This is the same function with arrow key a functionality of ES6

const square = (number) => number * number;

console.log('This is The Result->', square(5));

// Another Exaple of arrow fnc using job array
const jobs = [ { id: 1, is_active: true }, { id: 1, is_active: true }, { id: 1, is_active: false } ];

// Normal Function
const activeJobss = jobs.filter(function(job) {
	return job.is_active;
});
// Arrow function
const activeJobs = jobs.filter((job) => job.is_active);

console.log(activeJobs);

const colors = [ 'red', 'green' ];
const items = colors.map((color) => '<li>' + color + '</li>');
// With a templayte literal intro in ES6
const items_temp = colors.map((color) => `<li> + ${color} + </li>`);

console.log(items_temp);

// Object Destructuring

const address = {
	street: '',
	city: ''
};

const streets = address.street;
const citys = address.city;
//  Now with Destructuring
const { street, city } = address;
// If we want to give aliases
const { street: st } = address;
// The spread Operator to combine two arrays
const first = [ 1, 2 ];
const last = [ 4, 6 ];
const combined = [ ...first, ...last ];

// We can add a new element in between these two arrays also
const newcomb = [ ...first, 'Between Element', ...last, 'End Element' ];

console.log(newcomb);
