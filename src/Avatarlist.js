import React from 'react';
import 'tachyons';

const Avatarlist = (props) => {
	return (
		<div className="avatarstyle ma4 bg-light-blue dib pa4 tc">
			<img src={`https://joeschmoe.io/api/v1/${props.name}`} alt="avatar" />
			<h1>{props.name}</h1>
			<p>{props.occ}</p>
		</div>
	);
};

export default Avatarlist;
